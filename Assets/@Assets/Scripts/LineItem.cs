﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sudoku
{
    public class LineItem : MonoBehaviour
    {
        public LineRenderer lineRenderer;

        public void Init(float startX, float endX, float startY, float endY)
        {
            lineRenderer.SetPosition(0, new Vector3(startX, startY, 0));
            lineRenderer.SetPosition(1, new Vector3(endX, endY, 0));
            
        }
    }
}