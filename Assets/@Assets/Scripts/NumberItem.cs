﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Sudoku
{
    
    public class NumberItem : MonoBehaviour
    {
        public Button button;
        public TextMeshProUGUI text;
        public RectTransform rectTransform;
        public int row, col;
        
        public void Init(float size, int row, int col)
        {
            this.row = row;
            this.col = col;
            text.gameObject.SetActive(false);
            rectTransform.sizeDelta = new Vector2(size, size);
        }

        public void Show(int number)
        {
            if (number != 0)
            {
                text.gameObject.SetActive(true);
                text.text = number.ToString();
            }
        }

        public void ShowResult(int number)
        {
            if (number != 0)
            {
                text.gameObject.SetActive(true);
                text.text = number.ToString();
                text.color = Color.blue;
            }
        }
    }
}