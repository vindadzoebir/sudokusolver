﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Sudoku
{
    public class NumberManager : MonoBehaviour
    {
        private List<NumberButton> btnNumbers = new List<NumberButton>();
        private List<NumberItem> itemNumbers = new List<NumberItem>();
        public NumberItem numberObj;
        public NumberButton numberBtn;
        public LineItem lineItem;
        public Transform parent, buttonParent;
        public Button calculateBtn;

        public GameObject loadingScreen;

        private NumberItem selectedNumber;

        private float cellSize = 0;


        private int[,] array;
        private int gridSize;

        public void Construct()
        {
            Init();
        }
        
        void Init()
        {
            gridSize = GameManager.Instance.SelectedGrid;
            
            array = new int[gridSize,gridSize];
            loadingScreen.SetActive(false);
            CreateNumberButtons();
            ShowGrid(array);
            calculateBtn.OnClickAsObservable().TakeUntilDestroy(this).Subscribe(x =>
            {
                loadingScreen.SetActive(true);

                StartCoroutine(OnCalculate());

            });
        }

        void ShowGrid(int [,] array)
        {
//            int row = array.GetLength(0);
//            int column = array.GetLength(1);
                
            cellSize = (1080 - 200) / gridSize;
            float startX = (1080 - (gridSize * cellSize)) / 2f ;
            float startY = (-1920 + (gridSize * cellSize)) / 2f ;

            for (int i = 0; i < gridSize; i++)
            {
                for (int j = 0; j < gridSize; j++)
                {
                    NumberItem item = Instantiate(numberObj);
                    item.transform.SetParent(parent, false);
                    item.Init(cellSize, i, j);
                    item.transform.localPosition = new Vector3(startX + (j * cellSize), startY - (i * cellSize), 0);
                    item.Show(array[i,j]);
                    itemNumbers.Add(item);
                    
                    item.button.OnClickAsObservable().TakeUntilDestroy(item.gameObject).Subscribe(x =>
                    {
                        OnSelectCell(item);
                    });

                }
            }
            
//            CreateLines();
        }

        void CreateLines()
        {
            LineItem line1 = Instantiate(lineItem);
            line1.transform.SetParent(parent, false);
            line1.Init(itemNumbers[3].transform.localPosition.x, itemNumbers[3].transform.localPosition.x, itemNumbers[3].transform.localPosition.y, itemNumbers[80].transform.localPosition.y - cellSize);
            
            LineItem line2 = Instantiate(lineItem);
            line2.transform.SetParent(parent, false);
            line2.Init(itemNumbers[6].transform.localPosition.x, itemNumbers[6].transform.localPosition.x, itemNumbers[3].transform.localPosition.y, itemNumbers[80].transform.localPosition.y - cellSize);
            
            LineItem line3 = Instantiate(lineItem);
            line3.transform.SetParent(parent, false);
            line3.Init(itemNumbers[0].transform.localPosition.x, itemNumbers[8].transform.localPosition.x + cellSize, itemNumbers[27].transform.localPosition.y, itemNumbers[27].transform.localPosition.y);
            
            LineItem line4 = Instantiate(lineItem);
            line4.transform.SetParent(parent, false);
            line4.Init(itemNumbers[0].transform.localPosition.x, itemNumbers[8].transform.localPosition.x + cellSize, itemNumbers[54].transform.localPosition.y, itemNumbers[54].transform.localPosition.y);
        }

        void OnSelectCell(NumberItem item)
        {
            ShowButtons(true);
            selectedNumber = item;
        }

        void CreateNumberButtons()
        {
//            int maxNumber = gridSize;

            for (int i = 0; i <= gridSize; i++)
            {
                NumberButton item = Instantiate(numberBtn);
                item.transform.SetParent(buttonParent, false);
                item.Init(i);
                item.button.OnClickAsObservable().TakeUntilDestroy(item.gameObject).Subscribe(x =>
                {
                    SelectNumberButton(item.setNumber);
                });
                btnNumbers.Add(item);
            }
            
            btnNumbers[0].transform.SetAsLastSibling();

            ShowButtons(false);
        }

        void SelectNumberButton(int setNumber)
        {
            ShowButtons(false);
            
            selectedNumber.Show(setNumber);

            array[selectedNumber.row, selectedNumber.col] = setNumber;

            //change array value from here
        }

        void ShowButtons(bool show)
        {
            buttonParent.gameObject.SetActive(show);
        }

        IEnumerator OnCalculate()
        {
            yield return new WaitForSeconds(0.1f);
            Debug.Log("Start calculate");
            Backtracking backtrack = new Backtracking();
            backtrack.OnCalculate(array, delegate(int[,] result) { 
                ShowResults(result);
                loadingScreen.SetActive(false);
             });
        }

        void ShowResults(int[,] result)
        {
            int count = itemNumbers.Count;

            for (int i = 0; i < count; i++)
            {
                int row = i / 9;
                int col = i % 9;

                itemNumbers[i].ShowResult(result[row, col]);
            }
        }
    }
}