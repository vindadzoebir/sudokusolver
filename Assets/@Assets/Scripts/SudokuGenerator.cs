﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Sudoku
{
    public class SudokuGenerator : MonoBehaviour
    {
        public static int boxSize = 3;
        public static int boxCount = boxSize * boxSize;
        
        List<int> elligibleNumbers = new List<int>();
        
        int[,] array = new int[boxCount,boxCount];

        public Transform parent;
        public GameObject objPrefab;
        
        
        void Start()
        {
            GenerateElligibleNumbers();
            GenerateRandomNumbers();
        }

        void GenerateElligibleNumbers()
        {
            for (int i = 0; i < boxCount; i++)
            {
                elligibleNumbers.Add(i+1);
            }
        }

        void GenerateRandomNumbers()
        {
            for (int i = 0; i < boxCount; i++)
            {
                for (int j = 0; j < boxCount; j++)
                {
                    array[i, j] = FindEligibleNumber(i, j);
                }
            }

            ShowResults();
            string showArray = string.Empty;
            for (int i = 0; i < boxCount; i++)
            {
                for (int j = 0; j < boxCount; j++)
                {
                    showArray += String.Concat(array[i, j], ", ");
                }

                showArray += "\n";
            }
            Debug.Log(showArray); 
        }

        int FindEligibleNumber(int row, int column)
        {
            List<int> newRandom = new List<int>(elligibleNumbers);
            //Debug.Log("Row : " + row + ", column : " + column);
            
            for (int i = 0; i <= row; i++)
            {
                if (newRandom.Contains(array[i, column]))
                    newRandom.Remove(array[i, column]);
            }
            
            for (int j = 0; j <= column; j++)
            {
                //Debug.Log("Array [" + i + ", " + j + "] " + array[i,j]);
                if (newRandom.Contains(array[row, j]))
                    newRandom.Remove(array[row, j]);
            }

            if (row % boxSize != 0 || column % boxSize != 0)
            {
                int startRow = (row / boxSize) * boxSize;
                int startColumn = (column / boxSize) * boxSize;
                
                Debug.Log(string.Format("Row : {0}, Column : {1}, Start Row : {2}, Start Column : {3}", row, column, startRow, startColumn));
                for (int i = startRow; i <= row; i++)
                {
                    for (int j = startColumn; j <= column; j++)
                    {
                        if (newRandom.Contains(array[i, j])) ;
                        newRandom.Remove(array[i, j]);
                    }
                }
            }
            
            string showArray = string.Empty;
            
            for (int j = 0; j < newRandom.Count; j++)
            {
                showArray += String.Concat(newRandom[j], ", ");
            }

           
           
            //Debug.Log(showArray);

            int number = Random.Range(0, newRandom.Count);
            //Debug.Log(number);

            if (newRandom.Count > 0)
            {
                //Debug.Log("Random : " + newRandom[number]);
                return newRandom[number];
            }
            else
            {

                return 0;
            }
        }

        void ShowResults()
        {
            float startX = 1080 - (100 * boxCount) / 2f;
            float startY = 1920 - (100 * boxCount) / 2f;

            for (int i = 0; i < boxCount; i++)
            {
                for (int j = 0; j < boxCount; j++)
                {
                    GameObject obj = GameObject.Instantiate(objPrefab);
                    obj.transform.SetParent(parent, false);
                    obj.transform.localPosition = new Vector3(0 + (j * 100), 0 - (i * 100), 0);
                    obj.GetComponent<TextMeshProUGUI>().text = array[i, j].ToString();
                }
            }
        }
    }
}