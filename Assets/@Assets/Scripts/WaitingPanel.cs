﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sudoku
{
    public class WaitingPanel : MonoBehaviour
    {
        private UIManager uiManager;
        public void Construct(UIManager uiManager)
        {
            this.uiManager = uiManager;
            Init();
        }

        void Init()
        {
            Hide();
        }

        public void Show()
        {
            if(!gameObject.activeSelf)
                gameObject.SetActive(true);
        }

        public void Hide()
        {
            if(gameObject.activeSelf)
                gameObject.SetActive(false);
        }
    }
}