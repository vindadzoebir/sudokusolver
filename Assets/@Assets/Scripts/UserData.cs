﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace Sudoku
{
    [System.Serializable]
    public class UserData
    {
        IntReactiveProperty selectedGrid = new IntReactiveProperty(0);

        public void Construct()
        {
            Init();
        }

        void Init()
        {
            LoadUserData();
            
            selectedGrid.Subscribe(x => { SaveUserData(); });
        }

        public void SaveUserData()
        {
            
        }

        void LoadUserData()
        {
            
        }
    }
}