﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

namespace Sudoku
{
    public class UIManager : MonoBehaviour
    {
        public MenuPanel menuPanel;
        public GamePanel gamePanel;
        public WaitingPanel WaitingPanel;

        private UIState currentState;
        public UIState CurrentState { get { return currentState; } }
        
        [CanBeNull] private Stack<UIState> popUpStack = new Stack<UIState>();

        public void Construct()
        {
            Init();   
        }

        void Init()
        {
            menuPanel.Construct(this);
            gamePanel.Construct(this);
            
            WaitingPanel.Construct(this);
            
            OnChangeState(UIState.Menu);
        }

        public void OnChangeState(UIState state)
        {
            OnExitState(currentState);
            currentState = state;
            OnEnterState(currentState);
        }

        public void OnPushPopup(UIState state)
        {
            if(!popUpStack.Contains(state))
                popUpStack.Push(state);
            
            OnShowPopup(state);
        }

        public void OnPopPopup()
        {
            if (popUpStack.Count > 0)
            {
                UIState lastPopup = popUpStack.Peek();
                OnHidePopup(lastPopup);
                popUpStack.Pop();
            }
        }

        void OnEnterState(UIState state)
        {
            Debug.Log("Entering state : " + state);
            switch (state)
            {
                case UIState.Menu:
                    menuPanel.Show();
                    break;
                case UIState.Game:
                    gamePanel.Show();
                    break;
            }
        }

        void OnExitState(UIState state)
        {
            Debug.Log("Exiting state : " + state);
            switch (state)
            {
                case UIState.Menu:
                    menuPanel.Hide();
                    break;
                case UIState.Game:
                    gamePanel.Hide();
                    break;
            }
        }

        void OnShowPopup(UIState state)
        {
            switch (state)
            {
                case UIState.Waiting:
                    WaitingPanel.Show();
                    break;
                    
            }
        }

        void OnHidePopup(UIState state)
        {
            switch (state)
            {
                case UIState.Waiting:
                    WaitingPanel.Hide();
                    break;
            }
        }
    }
}