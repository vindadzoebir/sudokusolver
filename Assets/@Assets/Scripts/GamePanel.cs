﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sudoku
{
    public class GamePanel : MonoBehaviour
    {
        private UIManager uiManager;
        public NumberManager numberManager;
        public void Construct(UIManager uiManager)
        {
            this.uiManager = uiManager;
            Init();
        }

        void Init()
        {
            Hide();
        }

        public void Show()
        {
            if (!gameObject.activeSelf)
                gameObject.SetActive(true);
            
            numberManager.Construct();
        }

        public void Hide()
        {
            if (gameObject.activeSelf)
                gameObject.SetActive(false);
        }
    }
}