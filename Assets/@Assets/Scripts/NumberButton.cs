﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Sudoku
{
    public class NumberButton : MonoBehaviour
    {
        public TextMeshProUGUI text;

        public int setNumber;

        public Button button;

        public void Init(int number)
        {
            setNumber = number;
            text.text = setNumber.ToString();
            
        }
    }
}