﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sudoku
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance;

        void Awake()
        {
            Instance = this;
        }
        
        public UIManager uiManager;

        int selectedGrid;
        public int SelectedGrid { get { return selectedGrid; } }
        
        void Start()
        {
            Init();   
        }

        void Init()
        {
            uiManager.Construct();
        }

        public void SetGrid(int grid)
        {
            selectedGrid = grid;
        }
    }
}