﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sudoku;
using UnityEngine;
using TMPro;
using UnityEngine.Experimental.Rendering;

public class Backtracking
{
    public int[,] array;
    private int gridSize;

    public void OnCalculate(int [,] grid, Action<int [,]> finishcalculate )
    {
        gridSize = GameManager.Instance.SelectedGrid;
        
        array = new int[gridSize,gridSize];
        
        string text = string.Empty;
        
        for (int i = 0; i < gridSize; i++)
        {
            for (int j = 0; j < gridSize; j++)
            {
                text = string.Concat(text, ", ", grid[i, j]);
            }

            text = string.Concat(text, "\n");
        }
        
        Debug.Log(text);
        this.array = grid;
        bool solved = SolveGrid(array);

        finishcalculate(array);
//        ShowResults(array);
    }
    bool CheckGrid(int [,] grid)
    {
//        int length = gridSize;

        for (int i = 0; i < gridSize; i++)
        {
            for (int j = 0; j < gridSize; j++)
            {
                if (grid[i, j] == 0)
                    return false;
            }
        }

        return true;
    }

    
    bool SolveGrid(int [,] grid)
    {
        int row = 0;
        int column = 0;

        int maxNumber = gridSize * gridSize;
        
        for (int i = 0; i < maxNumber; i++)
        {
            row = i / gridSize;
            column = i % gridSize;

            if (grid[row, column] == 0)
            {
                for (int x = 1; x <= gridSize; x++)
                {
                    if (!CheckRow(row, x, grid) && !CheckColumn(column, x, grid) && !CheckSquare(row, column, x, grid))
                    {
                        grid[row, column] = x;
                        
                        if (CheckGrid(grid))
                            return true;

//                        if (SolveGrid(grid))
//                            return 0;
//                        else
//                        {
//                            array
//                        }
                        else
                        {
                            if (SolveGrid(grid))
                                return true;
                        }

//                        break;
                    }
                }

                //array[row, column] = 0;
//                Debug.Log("<color=blue>BREAK CAN'T FIND THE ANSWER IN : " + row + ", " + column + "</color>");

                break;
            }

        }

        grid[row, column] = 0;
        Debug.Log("<color=red>BACKTRACK : [" + row + ", " + column + "] </color>");

        return false;
    }

    bool CheckRow(int row, int number, int[,] grid)
    {
        //Debug.Log("Number : " + number);
        for (int i = 0; i < 9; i++) 
        {
            if(grid[row,i] == number)
            {
                //Debug.Log("<color=red>USED IN : " + i + ", number : " + array[row, i] + "</color>");
                return true;
            }
        }
        return false;
    }

    bool CheckColumn(int column, int number, int[,] grid)
    {
        for (int i = 0; i < 9; i++)
        {
            if (grid[i, column] == number)
            {
                //Debug.Log("<color=red>USED IN : " + i + ", number : " + array[column, i] + "</color>");
                return true;
            }
        }
        return false;
    }

    bool CheckSquare(int row, int column, int number, int[,] grid)
    {
        int squareRow = (row / 3) * 3;
        int squareColumn = (column / 3) * 3;

        for (int i = squareRow; i < squareRow + 3; i++)
        {
            for (int j = squareColumn; j < squareColumn + 3; j++)
            {
                if (grid[i, j] == number)
                {
                    //Debug.Log("<color=red>USED IN : " + i + ", number : " + array[i, j] + "</color>");
                    return true;
                }
            }
        }
        return false;
    }
    
    void ShowResults(int [,] grid)
    {
//        float startX = 1080 - (100 * 9) / 2f;
//        float startY = 1920 - (100 * 9) / 2f;
//
//        for (int i = 0; i < 9; i++)
//        {
//            for (int j = 0; j < 9; j++)
//            {
//                GameObject obj = GameObject.Instantiate(objPrefab);
//                obj.transform.SetParent(parent, false);
//                obj.transform.localPosition = new Vector3(0 + (j * 100), 0 - (i * 100), 0);
//                obj.GetComponent<TextMeshProUGUI>().text = grid[i, j].ToString();
//            }
//        }
//
////        numberManager.Init(grid);
    }
}
