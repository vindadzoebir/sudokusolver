﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Sudoku
{
    public class MenuPanel : MonoBehaviour
    {
        private UIManager uiManager;
        public Button button4, button6, button9, button16;

        public void Construct(UIManager uiManager)
        {
            this.uiManager = uiManager;
            Init();
        }
        
        public void Init()
        {
            button4.OnClickAsObservable().TakeUntilDestroy(this).Subscribe(x => { OnSelectGrid(4); });

            button6.OnClickAsObservable().TakeUntilDestroy(this).Subscribe(x => { OnSelectGrid(6); });

            button9.OnClickAsObservable().TakeUntilDestroy(this).Subscribe(x => { OnSelectGrid(9); });

            button16.OnClickAsObservable().TakeUntilDestroy(this).Subscribe(x => { OnSelectGrid(16); });
            
            Hide();
        }

        public void Show()
        {
            if(!gameObject.activeSelf)
                gameObject.SetActive(true);
        }

        public void Hide()
        {
            if(gameObject.activeSelf)
                gameObject.SetActive(false);
        }

        public void OnSelectGrid(int grid)
        {
            GameManager.Instance.SetGrid(grid);
            uiManager.OnChangeState(UIState.Game);
        }
    }
}